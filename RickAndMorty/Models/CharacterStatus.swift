//
//  CharacterStatus.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI

enum CharacterStatus: String, CaseIterable {
    case alive = "Alive"
    case dead = "Dead"
    case unknown = "unknown"
    
    var statusImage: Image {
        switch self {
        case .alive: return Image(systemName: "heart.circle.fill")
        case .dead: return Image(systemName: "heart.slash.circle.fill")
        case .unknown: return Image(systemName: "eye.trianglebadge.exclamationmark")
        }
    }
    
    var statusImageColor: Color {
        switch self {
        case .alive: return .green
        case .dead: return .pink
        case .unknown: return .gray
        }
    }
}
