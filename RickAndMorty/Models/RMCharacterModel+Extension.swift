//
//  RMCharacterModel+Extension.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import RickMortySwiftApi

private let filterFields = ["status", "species", "type", "gender", "origin", "location", "created"]

extension RMCharacterModel {
    
    var asListPair: [(String, String)] {
        let m = Mirror(reflecting: self)

        return m.children.map { (label: String?, value: Any) -> (String, String)? in
            guard let label = label else { return nil }
            if let value = value as? RMCharacterLocationModel, !value.name.isEmpty {
                return (label, value.name)
            } else if let value = value as? RMCharacterOriginModel, !value.name.isEmpty {
                return (label, value.name)
            } else if let value = value as? String, !value.isEmpty {
                return (label, value)
            } else { return nil }
        }
        .compactMap { $0 }
        .filter { (label, _) -> Bool in
            filterFields.contains(label)
        }
    }
    
}
