//
//  DataLoadState.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import Foundation

enum DataLoadState<T> {
    case loading
    case error(Error?)
    case loaded(data: T)
}
