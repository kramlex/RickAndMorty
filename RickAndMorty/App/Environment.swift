//
//  Environment.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import Foundation
import RickMortySwiftApi

final class Environment {
    
    static let shared = Environment()
    
    var client = RMClient()
    
}
