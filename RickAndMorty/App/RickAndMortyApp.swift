//
//  RickAndMortyApp.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI

@main
struct RickAndMortyApp: App {
    var body: some Scene {
        WindowGroup {
            MainTabView()
        }
    }
}
