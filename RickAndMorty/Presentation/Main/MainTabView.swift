//
//  MainTabView.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI

struct MainTabView: View {
    
    @State private var selectedTab: Tab = .characters
    
    var body: some View {
        TabView {
            CharactersView()
                .tabItem {
                    VStack {
                        R.image.iconCharacters.image
                        Text("Characters")
                    }
                }
                .tag(Tab.characters)
            EpisodesView()
                .tabItem {
                    VStack {
                        R.image.iconEpisodes.image
                        Text("Episodes")
                    }
                }
                .tag(Tab.episodes)
        }
    }
    
    enum Tab {
        case characters
        case episodes
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
