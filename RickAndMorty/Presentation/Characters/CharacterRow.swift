//
//  CharacterRow.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import Kingfisher

struct CharacterRow: View {
    
    let imageUrl: String
    let name: String
    let status: CharacterStatus
    
    var body: some View {
        VStack {
            HStack(spacing: 20) {
                KFImage(URL(string: imageUrl))
                    .placeholder {
                        R.image.placeholderImage.image
                            .resizable()
                            
                    }
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(RoundedRectangle(cornerRadius: 16))
                    
                HStack(spacing: 16) {
                    Text(name)
                    status.statusImage
                        .foregroundColor(status.statusImageColor)
                }
            }
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
            Divider()
                .padding(EdgeInsets(top: .zero, leading: 16, bottom: .zero, trailing: 16))
        }
        
    }
}

struct CharacterRow_Previews: PreviewProvider {
    static var previews: some View {
        CharacterRow(
            imageUrl: "",
            name: "Rick Sanchez",
            status: .alive
        )
        .previewLayout(.sizeThatFits)
    }
}
