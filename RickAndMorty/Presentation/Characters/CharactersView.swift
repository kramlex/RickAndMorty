//
//  CharactersView.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import RickMortySwiftApi

struct CharactersView: View {
    
    @State private var searchText: String = ""
    @StateObject private var viewModel = CharactersViewModel()
    
    var body: some View {
        NavigationView {
            
            ScrollView {
                LazyVStack {
                    ForEach(viewModel.characters, id: \.id) { character in
                        NavigationLink {
                            CharacterDetailsView(
                                characterName: character.name,
                                characterUrl: character.url
                            )
                        } label: {
                            character.row
                        }
                        .buttonStyle(.plain)
                    }
                }
            }
            .navigationTitle("Episodes")
            .navigationBarTitleDisplayMode(.inline)
            .searchable(text: $viewModel.searchText)
            
            
            /// with UIKit Actvity Indicator
//            RefreshableScrollView(onRefresh: {
//                await viewModel.refresh()
//            }, content: {
//                LazyVStack {
//                    ForEach(viewModel.characters, id: \.id) { character in
//                        NavigationLink {
//                            Text("Character")
//                        } label: {
//                            character.row
//                        }
//                        .buttonStyle(.plain)
//                    }
//                }
//            })
//            .navigationTitle("Episodes")
//            .navigationBarTitleDisplayMode(.inline)
//            .searchable(text: $viewModel.searchText)
            
            
        }
        .onAppear {
            viewModel.viewOnAppear()
        }
        
    }
}

private extension RMCharacterModel {
    
    var row: CharacterRow {
        .init(
            imageUrl: image,
            name: name,
            status: CharacterStatus(rawValue: status) ?? .unknown
        )
    }
    
}

struct CharactersView_Previews: PreviewProvider {
    static var previews: some View {
        CharactersView()
    }
}
