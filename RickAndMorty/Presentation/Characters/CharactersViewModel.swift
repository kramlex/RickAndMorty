//
//  CharactersViewModel.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import Combine
import RickMortySwiftApi

final class CharactersViewModel: ObservableObject {
    
    private var cancellable: AnyCancellable?
    
    @Published private var allCharacters: [RMCharacterModel] = []
    @Published var searchText: String = ""
    
    var characters: [RMCharacterModel] {
        if searchText.isEmpty { return allCharacters }
        return allCharacters.filter { $0.name.contains(searchText) }
    }
    var client: RMClient {
        Environment.shared.client
    }
    
    func viewOnAppear() {
        fetchCharacters()
    }
    
    func refresh() async {
        await withCheckedContinuation {continuation in
            fetchCharacters {
                continuation.resume()
            }
        }
    }
    
    private func fetchCharacters(completion: (() -> Void)? = nil) {
        cancellable = client
            .character()
            .getAllCharacters()
            .sink(
                receiveCompletion: { _ in
                    completion?()
                },
                receiveValue: { [weak self] characters in
                    DispatchQueue.main.async {
                        self?.allCharacters = characters
                    }
                }
            )

    }
    
}
