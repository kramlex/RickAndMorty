//
//  EpisodeRow.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import Kingfisher

struct EpisodeRow: View {
    
    let imageUrl: String
    let name: String
    let episode: String
    let airDate: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(spacing: 16) {
                KFImage(URL(string: imageUrl))
                    .placeholder {
                        R.image.placeholderImage.image
                            .resizable()
                    }
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(RoundedRectangle(cornerRadius: 16))
                VStack(alignment: .leading, spacing: 5) {
                    Text(name)
                    Text(episode)
                        .font(.caption2)
                        .foregroundColor(.gray)
                    Text(airDate)
                        .font(.caption)
                        .foregroundColor(.gray)
                }
            }
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
            Divider()
                .padding(EdgeInsets(top: .zero, leading: 16, bottom: .zero, trailing: 16))
        }
    }
}

struct EpisodeRow_Previews: PreviewProvider {
    static var previews: some View {
        EpisodeRow(
            imageUrl: "",
            name: "Pilot",
            episode: "1SE1",
            airDate: "13 December, 2013"
        )
            .previewLayout(.sizeThatFits)
    }
}
