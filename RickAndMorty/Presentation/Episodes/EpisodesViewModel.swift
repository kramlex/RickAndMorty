//
//  EpisodesViewModel.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import RickMortySwiftApi
import Combine

final class EpisodesViewModel: ObservableObject {
    
    @Published private var allEpisodes: [RMEpisodeModel] = []
    @Published var searchText: String = ""
    private var cancellable: AnyCancellable?
    
    var episodes: [RMEpisodeModel] {
        if searchText.isEmpty { return allEpisodes}
        return allEpisodes.filter { $0.name.contains(searchText)}
    }
    
    var client: RMClient {
        Environment.shared.client
    }
    
    func viewOnAppear() {
        fetchEpisodes()
    }
    
    func refresh() async {
        await withCheckedContinuation {continuation in
            fetchEpisodes {
                continuation.resume()
            }
        }
    }
    
    private func fetchEpisodes(completion: (() -> Void)? = nil) {
        cancellable = client
            .episode()
            .getAllEpisodes()
            .sink(
                receiveCompletion: { k in
                    completion?()
                },
                receiveValue: { [weak self] episodes in
                    self?.allEpisodes = episodes
                }
            )
    }
    
}
