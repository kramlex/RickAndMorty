//
//  EpisodesView.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import RickMortySwiftApi

struct EpisodesView: View {
    
    @StateObject private var viewModel = EpisodesViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVStack {
                    ForEach(viewModel.episodes, id: \.id) { episode in
                        NavigationLink {
                            Text("Episode")
                        } label: {
                            episode.row
                        }
                        .buttonStyle(.plain)
                    }
                }
            }
            .navigationTitle("Episodes")
            .navigationBarTitleDisplayMode(.inline)
            .searchable(text: $viewModel.searchText)
            
            /// with UIKit Actvity Indicator
//            RefreshableScrollView(onRefresh: {
//                await viewModel.refresh()
//            }, content: {
//
//            })
//            .navigationTitle("Episodes")
//            .navigationBarTitleDisplayMode(.inline)
//            .onAppear {
//                viewModel.viewOnAppear()
//            }
        }
        .onAppear {
            viewModel.viewOnAppear()
        }
        
    }
}

private extension RMEpisodeModel {
    var row: EpisodeRow {
        .init(imageUrl: "", name: name, episode: episode, airDate: airDate)
    }
}

struct EpisodesView_Previews: PreviewProvider {
    
    static var previews: some View {
        EpisodesView()
    }
}
