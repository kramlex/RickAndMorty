//
//  ActivityIndicator.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import UIKit

struct ActivityIndicator: UIViewRepresentable {
    public typealias UIView = UIActivityIndicatorView
    public var isAnimating: Bool = true
    public var configuration = { (indicator: UIView) in }
    
    public init(isAnimating: Bool, configuration: ((UIView) -> Void)? = nil) {
        self.isAnimating = isAnimating
        if let configuration = configuration {
            self.configuration = configuration
        }
    }
    
    public init(configuration: ((UIView) -> Void)? = nil) {
        if let configuration = configuration {
            self.configuration = configuration
        }
    }
    
    public func makeUIView(context: UIViewRepresentableContext<Self>) -> UIView {
        UIView()
    }
    
    public func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<Self>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
        configuration(uiView)
    }
}

private typealias ViewWithModifier = ActivityIndicator
extension ViewWithModifier {
    
    func isAnimating(_ isAnimating: Bool) -> Self {
        return Self(isAnimating: isAnimating, configuration: configuration)
    }
    
}
