//
//  RefreshableScrollView.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI

// MARK: - RefreshableScrollView https://swiftuirecipes.com/blog/pull-to-refresh-with-swiftui-scrollview (Modify)

/// Tracks the state of the RefreshableScrollView - it's either:
/// 1. waiting for a scroll to happen
/// 2. has been primed by pulling down beyond THRESHOLD
/// 3. is doing the refreshing.
private enum RefreshState {
    case waiting, primed, loading
}

/// The offset threshold. 50 is a good number, but you can play
/// with it to your liking.

private let THRESHOLD: CGFloat = 50

struct RefreshableScrollView<Content: View>: View {
    /// the refreshing action
//    let onRefresh: OnRefresh
    let onRefresh: () async -> Void
    /// the ScrollView content
    let content: Content
    
    /// the current state
    @State private var state = RefreshState.waiting
    
    @State private var opacity: Double = 0.0
    
    /// activity indicator view
    private var activityIndicator = ActivityIndicator {
        $0.hidesWhenStopped = false
        $0.startAnimating()
    }
                                                       
    /// We use a custom constructor to allow for usage of a @ViewBuilder for the content
    init(onRefresh: @escaping () async -> Void, @ViewBuilder content: () -> Content) {
        self.onRefresh = onRefresh
        self.content = content()
    }
    
    private func stopLoading() {
        withAnimation {
            self.state = .waiting
            self.opacity = 0.0
        }
    }
    
    var body: some View {
        /// The root view is a regular ScrollView
        ScrollView {
            /// The ZStack allows us to position the PositionIndicator,
            /// the content and the loading view, all on top of each other.
            ZStack(alignment: .top) {
                /// The moving positioning indicator, that sits at the top
                /// of the ScrollView and scrolls down with the content
                PositionIndicator(type: .moving)
                    .frame(height: 0)
                
                /// Your ScrollView content. If we're loading, we want
                /// to keep it below the loading view, hence the alignmentGuide.
                content
                    .alignmentGuide(.top, computeValue: { _ in
                        (state == .loading) ? -THRESHOLD : 0
                    })
                
                /// The loading view. It's offset to the top of the content unless we're loading.
                ZStack {
                    Rectangle()
                        .foregroundColor(.white)
                        .frame(height: THRESHOLD)
                    activityIndicator
                        .opacity(opacity)
                }.offset(y: (state == .loading) ? 0 : -THRESHOLD)
            }
        }
        /// Put a fixed PositionIndicator in the background so that we have
        /// a reference point to compute the scroll offset.
        
        .background(PositionIndicator(type: .fixed))
        
        /// Once the scrolling offset changes, we want to see if there should
        /// be a state change.
        .onPreferenceChange(PositionPreferenceKey.self) { values in
            if state != .loading {
                /// If we're already loading, ignore everything
                /// Map the preference change action to the UI thread
                DispatchQueue.main.async {
                    
                    /// Compute the offset between the moving and fixed PositionIndicators
                    let movingY = values.first { $0.type == .moving }?.y ?? 0
                    let fixedY = values.first { $0.type == .fixed }?.y ?? 0
                    let offset = movingY - fixedY
                    
                    
                    if state != .loading {
                        let opacityWithoutRounding = offset / THRESHOLD
                        if opacityWithoutRounding < 0 {
                            self.opacity = 0.0
                        } else if opacityWithoutRounding >= 1 {
                            self.opacity = 1.0
                        } else {
                            self.opacity = opacityWithoutRounding
                        }
                    }
                    /// If the user pulled down below the threshold, prime the view
                    if offset > THRESHOLD && state == .waiting {
                        withAnimation {
                            self.state = .primed
                        }
                        /// If the view is primed and we've crossed the threshold again on the
                        /// way back, trigger the refresh
                    } else if offset < THRESHOLD && state == .primed {
                        state = .loading
                        Task {
                            await onRefresh()
                            stopLoading()
                        }
                    }
                }
            }
        }
    }
}

// MARK: - Position Indicator

private enum PositionType {
    case fixed, moving
}

/// This struct is the currency of the Preferences, and has a type
/// (fixed or moving) and the actual Y-axis value.
/// It's Equatable because Swift requires it to be.
private struct Position: Equatable {
    let type: PositionType
    let y: CGFloat
}

struct OffsetPreferenceKey: PreferenceKey {
  static var defaultValue: CGFloat = .zero
  static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {}
}

/// This might seem weird, but it's necessary due to the funny nature of
/// how Preferences work. We can't just store the last position and merge
/// it with the next one - instead we have a queue of all the latest positions.
private struct PositionPreferenceKey: PreferenceKey {
    typealias Value = [Position]
    
    static var defaultValue = [Position]()
    
    static func reduce(value: inout [Position], nextValue: () -> [Position]) {
        value.append(contentsOf: nextValue())
    }
}

private struct PositionIndicator: View {
    let type: PositionType
    
    var body: some View {
        GeometryReader { proxy in
            /// the View itself is an invisible Shape that fills as much as possible
            Color.clear
            /// Compute the top Y position and emit it to the Preferences queue
                .preference(key: PositionPreferenceKey.self, value: [Position(type: type, y: proxy.frame(in: .global).minY)])
        }
    }
}
