//
//  ScrollRefreshable.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI

struct ScrollRefreshable<Content:View>: View {
    
    var content: Content
    var onRefresh: (() async -> Void)?
    
    init(
        title: String? = nil,
        onRefresh: (() async -> Void)? = nil,
        @ViewBuilder content: @escaping () -> Content
    ) {
        self.content = content()
        self.onRefresh = onRefresh
        
        if let title = title {
            UIRefreshControl.appearance().attributedTitle =
                NSAttributedString(string: title)
        }
    }
    
    
    var body: some View {
        List {
            content
                .listRowSeparator(.hidden)
                .listRowBackground(Color.clear)
                .listRowInsets(
                    EdgeInsets(
                        top: .zero,
                        leading: .zero,
                        bottom: .zero,
                        trailing: .zero
                    )
                )
        }
        .listStyle(.plain)
        .refreshable {
            await onRefresh?()
        }
    }
}
