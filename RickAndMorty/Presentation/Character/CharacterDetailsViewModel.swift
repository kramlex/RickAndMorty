//
//  CharacterDetailsViewModel.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import Foundation
import RickMortySwiftApi
import Combine

final class CharacterDetailsViewModel: ObservableObject {
    
    private let characterUrl: String
    private var cancellable: AnyCancellable?
    
    @Published var dataState: DataLoadState<RMCharacterModel> = .loading
    @Published var title: String
    
    var client: RMClient {
        Environment.shared.client
    }
    
    init(
        characterName: String,
        characterUrl: String
    ) {
        self.characterUrl = characterUrl
        self.title = characterName
    }
    
    func viewOnAppear() {
        fetchCharacterData()
    }
    
    private func fetchCharacterData() {
        cancellable = client
            .character()
            .getCharacterByURL(url: characterUrl)
            .sink(
                receiveCompletion: { [weak self] result in
                    switch result {
                    case .finished: break
                    case.failure(let error): self?.dataState = .error(error)
                    }
                },
                receiveValue: { [weak self] character in
                    DispatchQueue.main.async {
                        self?.dataState = .loaded(data: character)
                    }
                }
            )
    }
}
