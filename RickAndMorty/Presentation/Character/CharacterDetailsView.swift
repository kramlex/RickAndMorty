//
//  CharacterDetailsView.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import SwiftUI
import Kingfisher

struct CharacterDetailsView: View {
    
    @StateObject private var viewModel: CharacterDetailsViewModel
    
    init(
        characterName: String,
        characterUrl: String
    ) {
        let viewModel = CharacterDetailsViewModel(
            characterName: characterName,
            characterUrl: characterUrl
        )
        _viewModel = StateObject(wrappedValue: viewModel)
    }
    
    var body: some View {
        
        ScrollView {
            switch viewModel.dataState {
            case .error(let error):
                Text(error?.localizedDescription ?? "Some error")
            case .loaded(let character):
                VStack (alignment: .leading) {
                    KFImage(URL(string: character.image))
                        .placeholder {
                            R.image.placeholderImage.image
                                .resizable()
                        }
                        .roundCorner(radius: Radius.point(16))
                        .overlay(
                            RoundedRectangle(cornerRadius: 16,style: .circular)
                                .stroke(Color.gray, lineWidth: 4.0)
                                
                        )
                        .frame(maxWidth: .infinity)
                        .aspectRatio(contentMode: .fit)
                    
                    HStack(alignment: .firstTextBaseline) {
                        Text(character.name)
                            .font(.largeTitle)
                            .fontWeight(.ultraLight)
                        let characterStatus = CharacterStatus(
                            rawValue: character.status)
                         ?? .unknown
                        characterStatus.statusImage
                            .foregroundColor(characterStatus.statusImageColor)
                    }
                    Divider()
                        .font(.subheadline)
                        .foregroundColor(.gray)
                    VStack(alignment: .leading,spacing: 10) {
                        Text("Information")
                            .underline()
                        let keyValueList = character.asListPair
                        ForEach(keyValueList, id: \.0) { keyValue in
                            HStack {
                                Text(keyValue.0.capitalizingFirstLetter())
                                    .font(.footnote)
                                Spacer()
                                Text(keyValue.1)
                                    .font(.footnote)
                            }
                            .frame(maxWidth: .infinity)
                        }
                    }
                    
                    Spacer()
                }
                .padding()
            case .loading:
                ActivityIndicator(isAnimating: true)
            }
        }
        .onAppear {
            viewModel.viewOnAppear()
        }
        .navigationTitle(viewModel.title)
    }
}

struct CharacterDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterDetailsView(
            characterName: "Rick Sanchez", characterUrl: "https://rickandmortyapi.com/api/character/1")
    }
}
