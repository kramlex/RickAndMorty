//
//  EpisodeImageLinker.swift
//  RickAndMorty
//
//  Created by mdubkov on 05.04.2022.
//

import Foundation

enum EpisodeImageLinker {
    
    
    
    case episode: (season: Int, episode: Int)
    
    var imageUrl: String {
        
    }
}
